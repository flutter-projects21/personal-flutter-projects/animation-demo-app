## Cat Player

This is a simple flutter animation demo project which I created in order to implement various animation techniques.


## 1. Using PageRouteBuilder:
   By using this widget, we can animate the Navigation between two screens.

![navigation](readme_images/navigation.gif)

## 2. Using AnimatedBuilder:

![navigation](readme_images/animated%20builder.gif)

## 3. Using Rive Animations:

![navigation](readme_images/rive.gif)
![navigation](readme_images/rive2.gif)

## 4. Using lottie Animations:

![navigation](readme_images/lottie.gif)
