import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rive/rive.dart';
import 'dart:math' as math;

class MusicPlayerPage extends StatefulWidget {
  const MusicPlayerPage({Key? key}) : super(key: key);

  @override
  _MusicPlayerPageState createState() => _MusicPlayerPageState();
}

class _MusicPlayerPageState extends State<MusicPlayerPage> {
  late RiveAnimationController _prevButtonController;
  late RiveAnimationController _nextButtonController;
  late RiveAnimationController _soundWaveController;

  SMIInput<bool>? _playButtonInput;

  Artboard? _playButtonArtboard;

  void _playTrackChangeAnimation(RiveAnimationController controller) {
    if (controller.isActive == false) {
      print("test");
      controller.isActive = true;
    }
  }

  void _playPauseButtonAnimation() {
    if (_playButtonInput?.value == false &&
        _playButtonInput?.controller.isActive == false) {
      _playButtonInput?.value = true;
      _toggleWaveAnimation();
      animController.repeat();
    } else if (_playButtonInput?.value == true &&
        _playButtonInput?.controller.isActive == false) {
      _playButtonInput?.value = false;
      _toggleWaveAnimation();
      animController.stop();
    }
  }

  void _toggleWaveAnimation() => setState(
      () => _soundWaveController.isActive = !_soundWaveController.isActive);

  @override
  void initState() {
    super.initState();

    _prevButtonController = OneShotAnimation(
      'onPrev',
      autoplay: false,
    );
    _nextButtonController = OneShotAnimation(
      'onNext',
      autoplay: false,
    );

    _soundWaveController = SimpleAnimation(
      'loopingAnimation',
      autoplay: false,
    );

    rootBundle.load('assets/rivs/PlayPauseButton.riv').then((data) {
      print("init-----------------------");
      print(data);

      final file = RiveFile.import(data);
      final artboard = file.mainArtboard;
      var controller =
          StateMachineController.fromArtboard(artboard, 'PlayPauseButton');

      if (controller != null) {
        artboard.addController(controller);
        _playButtonInput = controller.findInput("isPlaying");
      }
      setState(() {
        _playButtonArtboard = artboard;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            AnimationPage(),
            SizedBox(
              height: 60,
            ),
            _playButtonArtboard == null
                ? SizedBox()
                : Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      GestureDetector(
                        onTapDown: (_) {
                          _playTrackChangeAnimation(_prevButtonController);
                        },
                        child: SizedBox(
                          width: 115,
                          height: 115,
                          child: RiveAnimation.asset(
                              "assets/rivs/PrevTrackButton.riv",
                              controllers: [_prevButtonController]),
                        ),
                      ),
                      GestureDetector(
                        onTapDown: (_) {
                          _playPauseButtonAnimation();
                        },
                        child: SizedBox(
                          height: 125,
                          width: 125,
                          child: Rive(
                            artboard: _playButtonArtboard!,
                            fit: BoxFit.fitHeight,
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTapDown: (_) {
                          _playTrackChangeAnimation(_nextButtonController);
                        },
                        child: SizedBox(
                          width: 115,
                          height: 115,
                          child: RiveAnimation.asset(
                              "assets/rivs/NextTrackButton.riv",
                              controllers: [_nextButtonController]),
                        ),
                      ),
                    ],
                  ),
            SizedBox(height: 40),
            Container(
              height: 100,
              width: 400,
              child: RiveAnimation.asset(
                'assets/rivs/SoundWave.riv',
                fit: BoxFit.contain,
                controllers: [_soundWaveController],
              ),
            )
          ],
        ),
      ),
    );
  }
}

late AnimationController animController;

class AnimationPage extends StatefulWidget {
  _AnimationPageState createState() => _AnimationPageState();
}

// Use TickerProviderStateMixin if you have multiple AnimationControllers
class _AnimationPageState extends State<AnimationPage>
    with SingleTickerProviderStateMixin {
  late Animation<double> animation;

  @override
  void initState() {
    super.initState();
    animController = AnimationController(
      duration: Duration(milliseconds: 5000),
      vsync: this,
    )..repeat();

    animation = Tween(begin: 0.0, end: 1.0).animate(animController);
    animController.stop();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(50.0),
      child: RotationTransition(
        child: Container(
          height: 300,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(
                'assets/images/cat_logo.jpg',
              ),
            ),
            shape: BoxShape.circle,
          ),
        ),
        turns: animation,
      ),
    );
  }

  @override
  void dispose() {
    animController.dispose();
    super.dispose();
  }
}

class RotatingTransition extends StatelessWidget {
  const RotatingTransition({required this.child, required this.angle});

  final Widget child;
  final Animation<double> angle;

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: angle,
      child: child,
      builder: (context, child) {
        return Transform.rotate(
          angle: angle.value,
          child: child,
        );
      },
    );
  }
}
