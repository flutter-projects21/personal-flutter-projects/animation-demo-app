import 'package:flutter/material.dart';
import 'package:my_animation_demo/lottie_screen.dart';
import 'package:provider/provider.dart';

class SecondPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: (){
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (_) => LottieScreen(),
              ));
        },
        child: Column(
          children: const <Widget>[
            Spacer(),
            Expanded(
              child: SlidingContainer(
                initialOffsetX: 1,
                intervalStart: 0,
                intervalEnd: 0.5,
                imageName: 'upper.png',
              ),
            ),
            Expanded(
              child: SlidingContainer(
                initialOffsetX: -1,
                intervalStart: 0.5,
                intervalEnd: 1,
                imageName: 'lower.png',
              ),
            ),
            Spacer(),

          ],
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Navigator.of(context).pop();
        },
        label: Text('Navigate Back'),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}

class SlidingContainer extends StatelessWidget {
  final double initialOffsetX;
  final double intervalStart;
  final double intervalEnd;
  final String imageName;

  const SlidingContainer({
    Key? key,
    required this.initialOffsetX,
    required this.intervalStart,
    required this.intervalEnd,
    required this.imageName,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final animation = Provider.of<Animation<double>>(context, listen: false);

    return AnimatedBuilder(
      animation: animation,
      builder: (context, child) {
        return SlideTransition(
          position: Tween<Offset>(
            begin: Offset(initialOffsetX, 0),
            end: Offset(0, 0),
          ).animate(CurvedAnimation(
              parent: animation,
              curve: Interval(intervalStart, intervalEnd,
                  curve: Curves.easeOutCubic))),
          child: child,
        );
      },
      child: Padding(
        padding: const EdgeInsets.only(left: 25),
        child: Image.asset('assets/images/$imageName'),
      ),
    );
  }
}
